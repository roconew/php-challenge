<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public function findByName($name = null): ?Contact
	{	
		return new Contact();
	}

	public function validateNumber(string $number): bool
	{
		return preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{3}$/", $number); 
	}
}