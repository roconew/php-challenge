<?php

namespace App;

use App\Interfaces\CarrierInterface;

class Carrier implements CarrierInterface
{
    public function dialContact(Contact $contact)
    {
        //code
    }

    public function makeCall(): Call
    {
        return new Call();
    }

    public function sendSms($body = null) : Sms
    {
       return new Sms($body);
    }
}