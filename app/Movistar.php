<?php

namespace App;

use App\Definitions\Carrier as Definition;

class Movistar extends Definition
{
    public function __construct()
    {
        parent::__construct('Movistar');
    }

    public function dialContact(Contact $contact)
    {
        //
    }

    public function makeCall(): Call
    {
        //
    }

    public function sendSms(string $body): Sms
    {
        //
    }
}