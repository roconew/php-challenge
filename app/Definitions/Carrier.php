<?php

namespace App\Definitions;

use App\Interfaces\CarrierInterface;

abstract class Carrier implements CarrierInterface
{
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
}