<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

	protected $provider;
	
	protected $service;

	function __construct(CarrierInterface $provider, ContactService $service)
	{
		$this->provider = $provider;
		$this->service = $service;

	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return null;

		$contact = $this->service->findByName($name);
		
		if (!$contact) return null;

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

	public function sendSms($number, $body)
	{
		if (!$number) return null;

		$isValid = $this->service->validateNumber($number);

		if (!$isValid) return null;

		return $this->provider->sendSms($body);
	}

}
