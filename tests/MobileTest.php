<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Mobile;
use App\Carrier;
use App\Call;
use App\Contact;
use App\Services\ContactService;


class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = new Carrier();
		$service = m::mock(ContactService::class);
		$service->shouldReceive('findByName')->with('')->andReturn(null);
	
		$mobile = new Mobile($provider, $service);

		$this->assertNull($mobile->makeCallByName(''));
	}
	/** @test */
	public function it_makes_a_valid_call_by_name()
	{
		$name = 'John Doe';
		$provider = new Carrier();
		$service = m::mock(ContactService::class);
		$service->shouldReceive('findByName')->with($name)->andReturn(new Contact());
		
		$mobile = new Mobile($provider, $service);

		$this->assertInstanceOf(Call::class, $mobile->makeCallByName($name));
	}

	/** @test */
	public function it_return_null_when_call_by_name_invalid()
	{
		$name = 'Jack Smith';
		$provider = new Carrier();
		$service = m::mock(ContactService::class);
		$service->shouldReceive('findByName')->with($name)->andReturn(null);
		$mobile = new Mobile($provider, $service);

		$this->assertNull($mobile->makeCallByName($name));
	}

	/** @test */
	public function it_returns_null_when_number_empty()
	{
		$provider = new Carrier();
		$service = m::mock(ContactService::class);
		$service->shouldReceive('validateNumber')->with('')->andReturn(false);
		$mobile = new Mobile($provider, $service);

		$this->assertNull($mobile->sendSms('','SMS Body'));
	}

    /** @test */
	public function it_sends_an_sms_valid_number()
    {
		$number = '917-927-428';
        $provider = new Carrier();
		$service = m::mock(ContactService::class);
		$service->shouldReceive('validateNumber')->with($number)->andReturn(true);
		
		$mobile = new Mobile($provider, $service);
		
        $this->assertNotNull($mobile->sendSms($number,'SMS Body'));
    }

    /** @test */
    public function it_returns_null_when_invalid_number()
    {
		$number = '12345678912@';
		$provider = new Carrier();
		$service = m::mock(ContactService::class);
		$service->shouldReceive('validateNumber')->with($number)->andReturn(false);
		
		$mobile = new Mobile($provider, $service);
        
        $this->assertNull($mobile->sendSms($number,'SMS Body'));
	}
	
}	
